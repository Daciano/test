FROM springci/graalvm-ce:master-java11

RUN ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime

VOLUME /tmp

ENV APP_NAME test.jar

COPY ["build/libs/test.jar",  "/application/"]

RUN bash -c 'touch /application/$APP_NAME'

WORKDIR  /application

EXPOSE 8080

# Start tomcat
ENTRYPOINT exec java -jar /application/$APP_NAME