package br.com.daciano.builders.test.controller;

import br.com.daciano.builders.test.assembler.ClientAssembler;
import br.com.daciano.builders.test.domain.Client;
import br.com.daciano.builders.test.dto.ClientDTO;
import br.com.daciano.builders.test.dto.ClientPageableDTO;
import br.com.daciano.builders.test.service.ClientService;
import com.querydsl.core.BooleanBuilder;

import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Log
@AllArgsConstructor
@Controller
public class ClientController {

  private final ClientAssembler clientAssembler;
  private final PagedResourcesAssembler<Client> pagedResourcesAssembler;
  private final ClientService clientService;

  @PostMapping(value = "/client", consumes = "application/json", produces = "application/json")
  public ResponseEntity<ClientDTO> create(@Valid @RequestBody ClientDTO client) {
    log.info("Post - Client "+client.toString());
    ClientDTO clientDTO = clientService.save(client.toDomain());
    log.info("Put - CREATED "+client.toString());
    return ResponseEntity.status(HttpStatus.CREATED).body(clientDTO);
  }

  @GetMapping("/client/{id}")
  public ResponseEntity<Client> getOrderById(@PathVariable String id) {
    log.info("Get - Client, id: "+id);
    Client client = clientService.getById(id);
    return ResponseEntity.status(HttpStatus.OK).body(client);
  }

  @PutMapping(value = "/client/{id}", consumes = "application/json", produces = "application/json")
  public ResponseEntity<?> put(@Valid @PathVariable String id, @Valid @RequestBody ClientDTO client) {
    log.info("Put - Client "+client.toString());
    return ResponseEntity.status(HttpStatus.CREATED).body(clientService.putUpdate(id, client));
  }

  @PatchMapping(value = "/client/{id}", consumes = "application/json", produces = "application/json")
  public ResponseEntity<?> patch(@Valid @PathVariable String id, @RequestBody ClientDTO client) {
    log.info("Patch - Client "+client.toString());
    return ResponseEntity.status(HttpStatus.ACCEPTED).body(clientService.patchUpdate(id, client));
  }

  @DeleteMapping(value = "/client/{id}")
  public ResponseEntity<?> remove(@Valid @PathVariable String id) {
    log.info("Delete - Client, id: "+id);
    clientService.delete(id);
    log.info("Delete - ACCEPTED, id: "+id);
    return ResponseEntity.status(HttpStatus.ACCEPTED).body("");
  }

  public ResponseEntity<CollectionModel<ClientPageableDTO>> getAll() {
    List<Client> clients = clientService.findAll();

    return new ResponseEntity<>(clientAssembler.toCollectionModel(clients), HttpStatus.OK);
  }

  @GetMapping("/client")
  public ResponseEntity<PagedModel<ClientPageableDTO>> getAll(
      Pageable pageable,
      @RequestParam(name = "name", required = false) String name,
      @RequestParam(name = "cpf", required = false) String cpf) {
    BooleanBuilder booleanBuilder = new BooleanBuilder();

    Page<Client> orderEntities = clientService.findAll(pageable, booleanBuilder, name, cpf);

    PagedModel<ClientPageableDTO> collModel =
        pagedResourcesAssembler.toModel(orderEntities, clientAssembler);

    return new ResponseEntity<>(collModel, HttpStatus.OK);
  }
}
