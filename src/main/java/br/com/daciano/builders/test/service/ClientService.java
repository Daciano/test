package br.com.daciano.builders.test.service;

import br.com.daciano.builders.test.domain.Client;
import br.com.daciano.builders.test.dto.ClientDTO;
import br.com.daciano.builders.test.exception.NotFoundException;
import br.com.daciano.builders.test.repository.ClientRepository;
import br.com.daciano.builders.test.repository.specification.CustomClientSpecification;
import com.querydsl.core.BooleanBuilder;


import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;


@AllArgsConstructor
@Service
public class ClientService {

  private final ClientRepository clientRepository;

  public List<Client> findAll() {
    return (List<Client>) clientRepository.findAll();
  }

  public Page<Client> findAll(Pageable pageable, BooleanBuilder booleanBuilder,String name, String cpf) {

    if (name != null && !name.isEmpty()) {
      booleanBuilder.and(CustomClientSpecification.name(name));
    }

    if (cpf != null && !cpf.isEmpty()) {
      booleanBuilder.and(CustomClientSpecification.cpf(cpf));
    }

    return clientRepository.findAll(booleanBuilder, pageable);
  }

  public ClientDTO save(Client Client) {
    return convertToDto(clientRepository.save(Client));
  }

  public void delete(String id) {
    clientRepository.delete(getById(id));
  }

  public Client getById(String id) {
    return  clientRepository.findById(id).orElseThrow(NotFoundException::new);
  }

  public ClientDTO putUpdate(String id, ClientDTO updatedClient) {
    Client client = getById(id);

    client.setCpf(updatedClient.getCpf());
    client.setName(updatedClient.getName());
    client.setBirthDate(updatedClient.getBirthDate());
    return save(client);
  }

  public ClientDTO patchUpdate(String id, ClientDTO updatedClient) {
    Client client = getById(id);

    if (updatedClient.getName().isBlank() ||  updatedClient.getName() != null)
      client.setName(updatedClient.getName());
    if (updatedClient.getName().isBlank() || updatedClient.getCpf() != null)
      client.setCpf(updatedClient.getCpf());
    if (updatedClient.getName().isBlank() || updatedClient.getBirthDate() != null)
      client.setBirthDate(updatedClient.getBirthDate());
    return save(client);
  }

  private ClientDTO convertToDto(Client client){
    ClientDTO clientDTO = new ClientDTO();

    clientDTO.setId(client.getId());
    clientDTO.setBirthDate(client.getBirthDate());
    clientDTO.setCpf(client.getCpf());
    clientDTO.setName(client.getName());
    clientDTO.setAge(client.getAge());

    return clientDTO;
  }
}
