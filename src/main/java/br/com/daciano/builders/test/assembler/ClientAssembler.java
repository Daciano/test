package br.com.daciano.builders.test.assembler;

import br.com.daciano.builders.test.controller.ClientController;
import br.com.daciano.builders.test.domain.Client;
import br.com.daciano.builders.test.dto.ClientPageableDTO;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class ClientAssembler extends RepresentationModelAssemblerSupport<Client, ClientPageableDTO> {

  public ClientAssembler() {
    super(ClientController.class, ClientPageableDTO.class);
  }

  @Override
  public ClientPageableDTO toModel(Client entity) {
    ClientPageableDTO clientPage = instantiateModel(entity);
    clientPage.add(linkTo(methodOn(ClientController.class).getOrderById(entity.getId())).withSelfRel());
    clientPage.id = entity.getId();
    clientPage.name = entity.getName();
    clientPage.cpf = entity.getCpf();
    clientPage.age = entity.getAge();
    return clientPage;
  }

  @Override
  public CollectionModel<ClientPageableDTO> toCollectionModel(Iterable<? extends Client> entities) {
    CollectionModel<ClientPageableDTO> orders = super.toCollectionModel(entities);
    orders.add(linkTo(methodOn(ClientController.class).getAll()).withSelfRel());

    return orders;
  }
}
