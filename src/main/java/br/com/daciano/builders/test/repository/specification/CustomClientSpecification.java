package br.com.daciano.builders.test.repository.specification;

import br.com.daciano.builders.test.domain.QClient;
import com.querydsl.core.types.Predicate;

public class CustomClientSpecification {

  public static Predicate name(String name) {
    return QClient.client.name.eq(name);
  }
  public static com.querydsl.core.types.Predicate cpf(String cpf) {
    return QClient.client.cpf.eq(cpf);
  }

}
