package br.com.daciano.builders.test.dto;

import br.com.daciano.builders.test.domain.Client;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;


@Data
public class ClientDTO {
    private String id;
    @NotBlank(message = "Nome vazio.")
    private String name;
    @NotBlank(message = "cpf vazio.")
    private String cpf;
    private int Age;
    @NotNull(message = "data de nascimento vazio.")
    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate birthDate;

    public Client toDomain(){
        Client client = new Client();
        client.setName(this.name);
        client.setBirthDate(this.birthDate);
        client.setCpf(this.cpf);
        return client;
    }
}
