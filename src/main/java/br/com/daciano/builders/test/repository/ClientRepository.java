package br.com.daciano.builders.test.repository;

import br.com.daciano.builders.test.domain.Client;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, String>, QuerydslPredicateExecutor<Client> {
}
