package br.com.daciano.builders.test.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

import java.time.LocalDate;


@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClientPageableDTO extends RepresentationModel<ClientPageableDTO> {

    public String id;
    public String name;
    public String cpf;
    @JsonFormat(pattern="dd/MM/yyyy")
    public LocalDate birtDate;
    public Integer age;
}
